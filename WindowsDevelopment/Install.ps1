﻿#requires -runasadministrator
#requires -version 4

$ErrorActionPreference = 'Stop'

function ValidateVisualStudioIsAlreadyInstalled()
{
    $requiredVisualStudioVersion = '14.0'
    if(!(Test-Path HKLM:\SOFTWARE\Microsoft\VisualStudio\$requiredVisualStudioVersion)) {
        throw "Expected Visual Studio $requiredVisualStudioVersion to be installed."
    }
}

function KillGitProcessThatStopsUpdatesFromWorking()
{
    Get-Process -ea 0 ssh-agent | Stop-Process -Force
}

function SetExecutionPolicy()
{
    Set-ExecutionPolicy RemoteSigned
}

function EnsureChocolateyInstalled()
{
    if(!(gcm cinst -ErrorAction SilentlyContinue)) {
        iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
        $env:path += "$($env:SYSTEMDRIVE)\chocolatey\bin"
    }
}

function SetHighPerformancePowerProfile()
{
    powercfg -setactive 8c5e7fda-e8bf-4a96-9a85-a6e23a8c635c
    if(!$?) {
        throw "Failed to set power plan to 'High performance'."
    }
}

function InstallChromeExtensions()
{
    $alreadyInstalled = ls -ea 0 (join-path ([Environment]::GetFolderPath('LocalApplicationData')) 'Google\Chrome\User Data\Default\Extensions') | select -expand Name

    'dbepggeogbaibhgnhhndojpepiihcmeb', <# Vimium #>
    'cfhdojbkjhnklbpkdaibdccddilifddb', <# Adblock Plus #>
    'febilkbfcbhebfnokafefeacimjdckgl', <# Markdown Preview Plus #>
    'mpbpobfflnpcgagjijhmgnchggcjblin', <# HTTP/2 and SPDY indicator #>
    'fhbjgbiflinjbdggehcddcbncdddomop'  <# Postman - REST Client (Packaged App) #> | % {
        if($alreadyInstalled -notcontains $_) {
            & "C:\Program Files (x86)\Google\Chrome\Application\Chrome.exe" --install-chrome-app=$_
        }
    }
}

function Update-PathVariablesForCurrentProcessFromRegistry
{
    [OutputType([Nullable])]
    param()

    function StripTrailingSemicolons([string]$Value)
    {
        if($Value) { $Value.TrimEnd(';') } else { $Value }
    }

    function UpdatePath([string][parameter(mandatory)]$EnvironmentVariableName, [string[]]$seed)
    {
        $newValues = $seed
        $newValues += StripTrailingSemicolons([System.Environment]::GetEnvironmentVariable($EnvironmentVariableName,'Machine')) -split ';'
        $newValues += StripTrailingSemicolons([System.Environment]::GetEnvironmentVariable($EnvironmentVariableName,'User')) -split ';'
        $newValue = StripTrailingSemicolons($newValues -join ';')

        $envName = "env:$EnvironmentVariableName"
        $currentValue = StripTrailingSemicolons((Get-Item -Path $envName).Value)
        if($currentValue -ne $newValue) {
            Set-Item -Path $envName -Value $newValue
            Write-Host -ForegroundColor Green "Updating $EnvironmentVariableName because the system variable was different to the local process variable, probably due to application installation."
            Write-Host
        }
    }

    UpdatePath PSModulePath ("$home\Documents\WindowsPowerShell\Modules", "$env:ProgramFiles\WindowsPowerShell\Modules")
    UpdatePath PATH @()
}

function InstallFromChocolatey()
{
    $tools =
        '7zip',
        '7zip.commandline',
        'beyondcompare',
        'dotPeek',
        'fiddler4',
        'filezilla',
        'Firefox',
        'gimp',
        'git /GitAndUnixToolsOnPath /NoAutoCrlf',
        'google-chrome-x64',
        'graphviz.portable',
        'hxd',
        'ilspy',
        'keepass',
        'kitty.portable',
        'linqpad4',
        'logparser',
        'microsoft-message-analyzer',
        'ncrunch-vs2015',
        'nmap',
        'nodejs',
        'obs',
        'perfview',
        'poshgit',
        #'resharper',    -- package is broken as at 2015-07-31
        'screentogif',
        'speccy',
        'superbenchmarker',
        'sysinternals',
        'treesizefree',
        'windbg',
        'wireshark'

    $chocPath = Join-Path $env:ChocolateyInstall 'choco.exe'
    $alreadyInstalled = & $chocpath list --local-only | ? { ($_ -split ' ').length -eq 2 } | % { ($_ -split ' ')[0] }
    $newTools = $tools | ? { $alreadyInstalled -notcontains ($_ -split ' ', 2)[0] }

    Write-Host "Updating all currently installed components..."
    Update-PathVariablesForCurrentProcessFromRegistry
    & $chocPath upgrade all -y
    Update-PathVariablesForCurrentProcessFromRegistry

    Write-Host "Installing all new components..."
    $newTools | % {
        $package, $packageParameters = $_ -split ' ', 2

        if($packageParameters) {
            & $chocPath install -y $package -params $packageParameters
        }
        else {
            & $chocPath install -y $package
        }

        Update-PathVariablesForCurrentProcessFromRegistry
    }
}

function InstallVisualStudioPathsToPowerShellProfile()
{
    Install-Package -Source psgallery pscx

    if((cat $profile) -contains 'ipmo pscx') {
        return
    }

    Add-Content -Path $profile -Value "`r`nipmo pscx`r`nInvoke-BatchFile `"`${env:VS140COMNTOOLS}\VsDevCmd.bat`"`r`n"
}

function CheckForResharperSettings()
{
    $fileName = 'ResharperSettings.DotSettings'

    if(!(Test-Path ~\AppData\Roaming\JetBrains\ReSharper)) {
        Write-Host "ReSharper does not appear to be installed. Skipping configuration."
        return
    }

    if(ls ~\AppData\Roaming\JetBrains\ReSharper\vAny\GlobalSettingsStorage.DotSettings -ea 0 | sls $fileName) {
        Write-Host "ReSharper settings appear to already be in place."
    }

    $fileLocation = Join-Path (Resolve-Path ~\AppData\Roaming) $fileName
    $wc = New-Object System.Net.WebClient
    $wc.DownloadFile("https://bitbucket.org/bartj/new-machine-setup/raw/HEAD/WindowsDevelopment/$fileName", $fileLocation)
    Write-Warning "You will need to manually install the ReSharper settings file at $fileLocation."
}

function ConfigureGit()
{
    git config --global core.autocrlf false
    git config --global core.longpaths true
    git config --global core.preloadindex true
    git config --global core.whitespace cr-at-eol
    git config --global mergetool.keepbackup false
    git config --global mergetool.prompt false
    git config --global difftool.prompt false
    git config --global rerere.enabled true
    git config --global receive.denynonfastforwards true
    git config --global core.logallrefupdates true
    git config --global push.default simple
    git config --global core.fscache true

    git config --global difftool.bc3.path "c:/program files (x86)/beyond compare 4/BCompare.exe"
    git config --global mergetool.bc3.path "c:/program files (x86)/beyond compare 4/BCompare.exe"

    git config --global difftool.vs.cmd 'devenv //diff """$LOCAL""" """$REMOTE"""'
    git config --global difftool.vs.keepbackup false
    git config --global difftool.vs.trustexitcode false

    git config --global difftool.vsdiffmerge.cmd 'vsdiffmerge.exe """$LOCAL""" """$REMOTE""" //t'
    git config --global difftool.vsdiffmerge.keepbackup false
    git config --global difftool.vsdiffmerge.trustexitcode false
    git config --global difftool.vsdiffmerge.prompt true    # this is required because vsdiffmerge exits immediately instead of waiting for VS to quit
    git config --global mergetool.vsdiffmerge.cmd 'vsdiffmerge.exe """$REMOTE""" """$LOCAL""" """$BASE""" """$MERGED""" //m'
    git config --global mergetool.vsdiffmerge.keepbackup false
    git config --global mergetool.vsdiffmerge.trustexitcode false
    git config --global mergetool.vsdiffmerge.prompt true    # this is required because vsdiffmerge exits immediately instead of waiting for VS to quit

    git config --global diff.tool bc3
    git config --global merge.tool bc3
}

function InstallPowerShellTimerPrompt()
{
    iex (New-Object Net.Webclient).DownloadString('https://gist.github.com/nzbart/5481021/raw/3-Installer.ps1')
}

function InstallVsixPackages()
{
    function InstallVsixPackage($packageName, $package)
    {
        $extensionAlreadyInstalled = ls -r ~\AppData\Local\Microsoft\VisualStudio\14.0\Extensions extension.vsixmanifest | cat | sls $packageName
        if($extensionAlreadyInstalled) {
            Write-Host "$packageName VSIX extension is already installed."
            return
        }

        Write-Host "Installing VSIX extension $packageName..."
        $url = "https://visualstudiogallery.msdn.microsoft.com/$package"
        $vsixInstaller = 'C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\VSIXInstaller.exe'
        $wc = New-Object System.Net.WebClient

        $tempFile = [IO.Path]::GetTempFileName()
        try {
            $wc.DownloadFile($url, $tempFile)
            $proc = Start-Process -PassThru -NoNewWindow -Wait $vsixInstaller '/q', $tempFile
            if($proc.ExitCode -ne 0) {
                throw "Failed to install VSIX extension: $packageName. Exit code = $($proc.ExitCode)."
            }
        }
        finally {
            rm $tempFile
        }
        Write-Host "Successfully installed $packageName."
    }

    InstallVsixPackage 'Visual Studio Spell Checker' a23de100-31a1-405c-b4b7-d6be40c3dfff/file/104494/5/VSSpellCheckerPackage.vsix
    #InstallVsixPackage 'Visual Studio Auto Updater' 14973bbb-8e00-4cab-a8b4-415a38d78615/file/150147/5/ExtensionUpdater.vsix                                          -- commented out until it supports Visual Studio 2015
    InstallVsixPackage 'ReSharper.AutoFormatOnSave' b5445a42-8c98-43cc-a4c5-7f7496f647c6/file/86257/9/ReSharper.AutoFormatOnSave.vsix
    InstallVsixPackage 'Web Essentials 2015' ee6e6d8c-c837-41fb-886a-6b50ae2d06a2/file/146119/19/WebEssentials2015.vsix
    #InstallVsixPackage 'Microsoft CodeLens Code Health Indicator' f85a7ab9-b4c2-436c-a6e5-0f06e0bac16d/file/112375/2/Microsoft.VisualStudio.CodeSense.CodeHealth.vsix -- commented out until it supports Visual Studio 2015
    InstallVsixPackage 'Fix File Encoding' 540ac2d8-f881-4794-8b00-810d28257b70/file/89822/3/FixFileEncoding_12.vsix
    InstallVsixPackage 'VSColorOutput' f4d9c2b5-d6d7-4543-a7a5-2d7ebabc2496/file/63103/9/VSColorOutput.vsix
}

function InstallSpellCheckDictionariesForVisualStudioSpellChecker()
{
    function ExtractFile($ExtensionFile)
    {
        Write-Host "Extracting dictionary files..."
        $extractedFolder = "$ExtensionFile.extracted"
        Add-Type -AssemblyName System.IO.Compression.FileSystem
        [System.IO.Compression.ZipFile]::ExtractToDirectory($ExtensionFile, $extractedFolder)
        $extractedFolder
    }

    function CopyFileToCorrectLocation($ExtractedFolder)
    {
        Write-Host "Copying dictionary files to local user dictionary folder..."
        $destination = "$env:LOCALAPPDATA\EWSoftware\Visual Studio Spell Checker"
        if(!(Test-Path $destination)) { md $destination | Out-Null }
        cp "$ExtractedFolder\*.aff" $destination
        cp "$ExtractedFolder\*.dic" $destination
    }

    function RemoveExtractedFiles($ExtractedFolder)
    {
        Write-Host "Removing temporary files..."
        rm -r -fo $ExtractedFolder
    }

    function ProcessDownloadedFile($ExtensionFile)
    {
        $extractedFolder = ExtractFile $ExtensionFile
        try {
            CopyFileToCorrectLocation $extractedFolder
        }
        finally {
            RemoveExtractedFiles $extractedFolder
        }
    }

    function DownloadFileAndProcess()
    {
        $downloadPage = Invoke-WebRequest 'http://extensions.openoffice.org/en/project/english-dictionaries-apache-openoffice'
        $latestVersionUrlPart =  $downloadPage.ParsedHtml.getElementsByTagName('a') | select -expand pathname | ? { $_ -match '^en/download/' } | select -first 1
        $latestVersionUrl = "http://extensions.openoffice.org/$latestVersionUrlPart"

        Write-Host "Downloading dictionary files from $latestVersionUrl..."
        $tempFile = [IO.Path]::GetTempFileName()
        try {
            $wc = New-Object System.Net.WebClient
            $wc.DownloadFile($latestVersionUrl, $tempFile)
            $wc.Dispose()
            ProcessDownloadedFile $tempFile
        }
        finally {
            rm $tempFile
        }
    }

    function SaveConfigurationFile()
    {
        Write-Host "Saving configuration file..."
        $content =
            '<?xml version="1.0" encoding="utf-8"?>
            <SpellCheckerConfiguration>
            <DefaultLanguage>en-GB</DefaultLanguage>
            <SpellCheckAsYouType />
            <IgnoreWordsWithDigits />
            <IgnoreFormatSpecifiers />
            <IgnoreFilenamesAndEMailAddresses />
            <IgnoreXmlElementsInText />
            <TreatUnderscoreAsSeparator />
            <ExcludeByFilenameExtension></ExcludeByFilenameExtension>
            <IgnoredCharacterClass>None</IgnoredCharacterClass>
            </SpellCheckerConfiguration>'

        Add-Type -AssemblyName System.Xml.Linq
        $formattedContent = [System.Xml.Linq.XDocument]::Parse($content).ToString()
        Set-Content -Path "$env:LOCALAPPDATA\EWSoftware\Visual Studio Spell Checker\SpellChecker.config" -Value $formattedContent
    }

    Write-Host "Installing en-GB spell check dictionary..."
    DownloadFileAndProcess
    SaveConfigurationFile
    Write-Host "Complete."
}

ValidateVisualStudioIsAlreadyInstalled
KillGitProcessThatStopsUpdatesFromWorking
SetExecutionPolicy
SetHighPerformancePowerProfile
EnsureChocolateyInstalled
InstallFromChocolatey
InstallVisualStudioPathsToPowerShellProfile
ConfigureGit
InstallChromeExtensions
CheckForResharperSettings
#InstallPowerShellTimerPrompt   -- commented out because it doesn't appear to work correctly on Windows 10
InstallVsixPackages
InstallSpellCheckDictionariesForVisualStudioSpellChecker
