#!/bin/bash

#Note: Simplest way to install is via Chocolatey's "cinst cyg-get"

#Install apt-cyg
wget https://github.com/john-peterson/apt-cyg/raw/path/apt-cyg
chmod +x apt-cyg
mv apt-cyg /bin/apt-cyg

#Install development tools - assumes that Git for Windows is already installed
apt-cyg install zsh mintty vim curl git-completion

#TODO: install VIM using my script or create symlinks to the Windows folders if already installed

# Create initial /etc/zshenv
[[ ! -e /etc/zshenv ]] && echo export PATH=/usr/bin:\$PATH > /etc/zshenv

#Install oh-my-zsh
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
if [ -f ~/.zshrc ] || [ -h ~/.zshrc ]
then
  cp ~/.zshrc ~/.zshrc.orig;
  rm ~/.zshrc;
fi
cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc

#Configure zsh as default (no chsh in cygwin)
sed -i "s/$USER\:\/bin\/bash/$USER\:\/bin\/zsh/g" /etc/passwd

zsh